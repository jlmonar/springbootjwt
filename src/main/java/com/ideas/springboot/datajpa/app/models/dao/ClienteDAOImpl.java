package com.ideas.springboot.datajpa.app.models.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

import com.ideas.springboot.datajpa.app.models.entity.Cliente;

/* Es una anotación de Spring para marcar la clase como componente de persistencia de acceso a datos. */
@Repository("ClienteDaoJPA")
public class ClienteDAOImpl {

	/*
	 * El entity manager se encarga de manejar las clases de entidades, el ciclo de
	 * vida, las persiste dentro del contexto, las actualiza, las elimina, puede
	 * realizar consultas, es decir, todas las operaciones a la base de datos pero a
	 * nivel de objetos, a través de las clases Entity. Por lo tanto, las consultas
	 * son siempre consultas JPA, va a la clase Entity, no a la tabla.
	 */
	/*
	 * Esta anotación @PersistenceContext va a inyectar automáticamente el
	 * EntityManager según la configuración de la unidad de persistencia que
	 * contiene el datasource, el proveedor jpa, por defecto si no configuramos
	 * ningún tipo de base de datos, ningún tipo de motor en application.properties,
	 * de forma automática SpringBoot va a utlizar el motor de base de datos H2
	 * embebida en la aplicación
	 */
	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	/*
	 * Esta anotación de Spring va a envolver el contenido del método dentro de una
	 * transacción.
	 */
	public List<Cliente> findAll() {
		return em.createQuery("from Cliente").getResultList();
	}

	public void save(Cliente cliente) {
		if (cliente.getId() != null && cliente.getId() > 0) {
			em.merge(cliente);// Actualiza si el cliente existe
		} else {
			em.persist(cliente);// Crea nuevo registro de cliente
		}
	}

	public Cliente findOne(Long id) {
		return em.find(Cliente.class, id);
	}

	public void delete(Long id) {
		Cliente cliente = findOne(id);
		em.remove(cliente);

	}

}
