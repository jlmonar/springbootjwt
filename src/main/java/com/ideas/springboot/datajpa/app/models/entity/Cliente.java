package com.ideas.springboot.datajpa.app.models.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "clientes")
public class Cliente implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/*
	 * Se puede omitir la anotación Column cuando el nombre de la columna en la
	 * tabla y el nombre del atributo de la clase son iguales
	 */
	@Column(name = "nombre")
	@NotEmpty
	private String nombre;

	/* Esta anotación indica que el campo no dese ser vacio. */
	@NotEmpty
	private String apellido;

	@NotEmpty
	/* Anotación que indica que el campo debe tener el formato de un email. */
	@Email
	private String email;

	@NotNull
	@Column(name = "create_at")
	/*
	 * La anotqación temporal permite convertir y formatear la fecha de la base de
	 * datos.
	 */
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	/*
	 * La anotación JsonFormat lo que hace es convertir el formato de la fecha que
	 * se mostrará en las respuestas JSON.
	 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date createAt;

	/*
	 * Esta anotación que el método se ejecute justo antes de invocar el método
	 * persist.
	 */
	/*
	 * @PrePersist public void prePersist() { createAt = new Date(); }
	 */
	private String foto;

	/*
	 * One se refiere a cliente y Many a muchas facturas. One se refiere a la clase
	 * en la cual estamos
	 */

	/*
	 * Cascade significa por ejemplo que todas las operaciones de lit y persist se
	 * van a realizar en cadena o cascada, por ejemplo, cuando el cliente guarde
	 * varias facturas o se le asignen varias facturas y el cliente se persiste con
	 * el método persist automáticamente va a persistir también a sus elementos
	 * hijos, o bien si se elimina un cliente también se van a eliminar de forma
	 * automática todas sus facturas, todos sus elementos hijos.
	 */
	/*
	 * Otro atributo importante es mappedBy donde asignamos el atributo de la otra
	 * clase de la relación, el cual sería el atributo del cliente, el nombre del
	 * atributo, el cual justamente se llama 'cliente'. Con mappedBy hacemos que sea
	 * bidireccional, es decir, cliente va a tener una lista de facturas, pero
	 * factura va a tener un cliente, va en ambos sentidos, Spring de forma
	 * automática va a crear la llave foránea clienteId en la tabla facturas con el
	 * fin de relacionar ambas tablas. Esta es una relación BIDIRECCIONAL.
	 */
	@OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	/*
	 * Debido a que es una relación bidireccional, al convertir a formato json se va
	 * a formar un json infinito ya que cada cliente va a tener sus facturas y a su
	 * vez estas facturas tendrán su cliente, lo que conlleva a un loop infinito.
	 * Para resolver esto usamos la anotación JsonIgnore, que lo que hace es oprimir
	 * el atributo 'facturas' al convertir a formato json.
	 */
	// @JsonIgnore
	/*
	 * Sin embargo, SI queremos mostrar las facturas de los clientes, para ello hay
	 * que resolver el problema del loop infinito en el json entre cliente y
	 * facturas, para resolver dicho problema usamos las anotaciones
	 * JsonManagedReference y JsonBackReference para que la relación sea
	 * unidireccional, es decir, solo desde cliente a factura y no de factura a
	 * cliente. Usamos la anotación JsonManagedReference en esta clase porque es la
	 * parte delantera normalmente la que se serializa en el json, mientras que
	 * JsonBackReference la usamos en la contraparte, es decir, en la parte que no
	 * queremos mostrar en el json.
	 */
	@JsonManagedReference
	private List<Factura> facturas;

	public Cliente() {
		facturas = new ArrayList<Factura>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public List<Factura> getFacturas() {
		return facturas;
	}

	public void setFacturas(List<Factura> facturas) {
		this.facturas = facturas;
	}

	public void addFactura(Factura factura) {
		facturas.add(factura);
	}

	@Override
	public String toString() {
		return nombre + " " + apellido;
	}
}
