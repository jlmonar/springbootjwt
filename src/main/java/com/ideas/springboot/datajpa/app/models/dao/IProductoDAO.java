package com.ideas.springboot.datajpa.app.models.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ideas.springboot.datajpa.app.models.entity.Producto;

public interface IProductoDAO extends CrudRepository<Producto, Long> {

	/*
	 * Primera forma de realizar la consulta, por medio de la anotación Query, esto
	 * permite realizar consultas a nivel de objetos Entity, no de tablas.
	 */
	@Query("select p from Producto p where p.nombre like %?1%")
	public List<Producto> findByNombre(String term);

	/*
	 * Segunda forma de realizar consulta, realizamos una consulta basada en el
	 * nombre del metodo, automáticamente spring data va a realizar la consulta
	 * simplemente por mantener un est´pandar en el nombre del método. Referencia:
	 * https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#
	 * repositories.query-methods.query-creation
	 */
	public List<Producto> findByNombreLikeIgnoreCase(String term);

	public List<Producto> findByNombreContainingIgnoreCase(String term);
}
