package com.ideas.springboot.datajpa.app.auth.filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ideas.springboot.datajpa.app.auth.service.JWTService;
import com.ideas.springboot.datajpa.app.auth.service.JWTServiceImpl;
import com.ideas.springboot.datajpa.app.models.entity.Usuario;

/*
 * Filtro que se encarga de hacer la autenticación, el cliente envia las credenciales 
 * (username y password) bajo cierta url o cierto tipo de petición(POST), este filtro
 * interceptor, se ejecuta en el request antes de llamar al controlador  y al método
 * handler, y va a realizar el login según las credenciales.
 */
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	private AuthenticationManager authenticationManager;
	private JWTService jwtService;

	public JWTAuthenticationFilter(AuthenticationManager authenticationManager, JWTService jwtService) {
		super();
		this.authenticationManager = authenticationManager;
		/*
		 * Este filtro JWTAuthenticationFilter se va a ejecutar SOLO cuando la ruta de
		 * la petición coincida con la ruta que se está especificando en el constructor
		 * de la superclase UsernamePasswordAuthenticationFilter y cuando la petición
		 * sea de tipo POST. Sin embargo, si queremos customizar nuestra ruta de login,
		 * lo podemos hacer usando el siguiente método de la superclase
		 * AbstractAuthenticationProcessingFilter.
		 */
		setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/api/login", "POST"));

		this.jwtService = jwtService;
	}

	/*
	 * Sobreescribimos este método porque es importante, ya que es quien se encarga
	 * de realizar la autenticación, o al menos de intentar realizar el login. Por
	 * debajo este método va a trabajar de la mano con nuestro proveedor de
	 * autenticación (JpaUserDetailsService). Este método por detrás de escena va a
	 * llamar al componente authenticationManager que trabajará de la mano con
	 * JpaUserDetailsService para realizar el login.
	 */
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		// Otra forma de obtener parametros enviados en el body del request.
		String username = request.getParameter("username");
		String password = obtainPassword(request);

		if (username != null && password != null) {
			/*
			 * Este bloque se ejecuta cuando el username y password son enviados por el
			 * cliente usando form-data
			 */
			logger.info("Username desde request parameter (form-data): " + username);
			logger.info("Password desde request parameter (form-data): " + password);
		} else {
			/*
			 * Este bloque se ejecuta cuando el username y password son enviados por el
			 * cliente usando raw, es decir, en el body de la petición (en formato json).
			 */
			Usuario user = null;

			try {
				/*
				 * Tenemos que convertir los datos que estamos recibiendo, los cuales están en
				 * request.getInputStream(), debemos convertirlo a un tipo Usuario, esta vez
				 * debemos convertir de un json a un objeto, para ello usamos el ObjectMapper,
				 * pero en esta ocasión en lugar de usar writeValueAsString usamos readValue()
				 * el cual recibe como primer argumento el origen, y segundo argumento el tipo
				 * de dato al que queremos convertir.
				 */
				user = new ObjectMapper().readValue(request.getInputStream(), Usuario.class);

				username = user.getUsername();
				password = user.getPassword();

				logger.info("Username desde request InputStream (raw): " + username);
				logger.info("Password desde request InputStream (raw): " + password);
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		username = username.trim();

		/*
		 * Creamos el UsernamePasswordAuthenticationToken que es quien se encarga de
		 * contener el contenedor que contiene las credenciales.
		 */
		/*
		 * Hay que tener en cuenta que el usernamePasswordAAuthenticationToken, no es el
		 * mismo token del JsonWebToken, usernamePasswordAAuthenticationToken se maneja
		 * de manera interna en nuestra aplicación con SpringSecurity, incluso a partir
		 * de este objeto podemos obtrener el username y todos los datos necesarios para
		 * crear el JsonWebToken.
		 */
		UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(username, password);

		/*
		 * authenticationManager es quien se encarga de realizar el login según nuestro
		 * proveedor de autenticación, JpaUserDetailsService en este caso. La
		 * autenticación se realiza con el token authToken.
		 */
		return authenticationManager.authenticate(authToken);
	}

	/*
	 * En esta función manejamos el éxito de una autenticación exitosa. Aquí tenemos
	 * como parametro el authResult que resulta ser el mismo authToken que tenemos
	 * en attemptAuthentication, pero la diferencia es que aquí en
	 * succesfulAuthentication ya está autenticado, pues tiene el atributo
	 * authenticated en true y además con todos los datos del usuario, username,
	 * roles y todo lo necesario.
	 */
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		// Creo el Json Web Token
		String token = jwtService.create(authResult);
		/*
		 * Pasamos el token en la cabecera de la respuesta para el usuario. Es
		 * importante que el nombre de este parámetro sea 'Authorization', empezando con
		 * mayúscula, e iniciado con el texto 'Bearer ' (con espacio) seguido del token
		 */
		response.addHeader(JWTServiceImpl.HEADER_STRING, JWTServiceImpl.TOKEN_PREFIX + token);

		/*
		 * Agregamos al body de la respuesta el token y algún otro parametro o atributo
		 * que queramos pasarle al cliente, esto en formato json, y para ello
		 * necesitamos un Map con los diferentes valores que queremos enviar al cliente.
		 */
		Map<String, Object> body = new HashMap<String, Object>();
		body.put("token", token);
		body.put("user", (User) authResult.getPrincipal());
		body.put("mensaje", String.format("Hola %s, has iniciado sesión con éxito!", authResult.getName()));

		/*
		 * Pasamos el Map body a la respuesta de la petición, para ello debemos obtener
		 * el writer (el escritor de la respuesta) e invocar al método write (escribir),
		 * donde debemos pasar el body, sin embargo, no podemos pasar el body
		 * directamente ya que es del tipo Map, debemos convertirlo antes a una
		 * estructura Json, para ello usamos el objeto ObjectWrapper() el cual nos
		 * permite convertir cualquier objeto Java en un Json.
		 */
		response.getWriter().write(new ObjectMapper().writeValueAsString(body));
		response.setStatus(200);
		response.setContentType("application/json");
	}

	/*
	 * En esta función manejamos las autenticaciones fallidas, y lo que hacemos es
	 * devolver un mensaje de error al cliente, en formato JSON, indicando cual fue
	 * el problema.
	 */
	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException {
		/*
		 * Agregamos los atributos de errores en el body para enviarlos al cliente.
		 */
		Map<String, Object> body = new HashMap<String, Object>();
		/*
		 * Importante por tema de seguridad, no especificar cual de los dos, si el
		 * username o el password es el incorrrecto, ya que estariamos dando información
		 * sensible a posibles hackers.
		 */
		body.put("mensaje", "Error de autenticación: username o password incorrectos!");
		/*
		 * Aquí agregamos un mensaje sobre el error, pero vamos a enviar el mensaje de
		 * error original que se maneja dentro de SpringSecurity, se encuentra dentro
		 * del objeto 'failed'.
		 */
		body.put("error", failed.getMessage());

		response.getWriter().write(new ObjectMapper().writeValueAsString(body));
		response.setStatus(401);
		response.setContentType("application/json");
	}
}
