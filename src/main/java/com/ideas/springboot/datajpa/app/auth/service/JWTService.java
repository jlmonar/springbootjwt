package com.ideas.springboot.datajpa.app.auth.service;

import java.io.IOException;
import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.jsonwebtoken.Claims;

/**
 * Interfaz la cual nos provee un servicio centralizado que nos permite trabajar
 * con json web tokens, ya sea para crear, validar, obtener username, claims,
 * roles o resolver el token, es decir, aqui se trabaja con todo lo nrelacionado
 * a JWT.
 * 
 * @author Jose Luis
 *
 */
public interface JWTService {

	/**
	 * Método que se encarga de crear un json web token.
	 * 
	 * @param auth El objeto authentication producto de una autenticación exitosa.
	 * @return El json web token.
	 */
	public String create(Authentication auth) throws IOException;

	/**
	 * Returna true si y solo si el token es valido.
	 * 
	 * @param token El token que queremos validar.
	 * @return true si el token es valido, false caso contrario.
	 */
	public boolean validate(String token);

	/**
	 * Método que obtiene los claims del token de un usuario autenticado.
	 * 
	 * @param token El token que contiene la información necesaria del usuario
	 *              autenticado
	 * @return Los claims contenidos token.
	 */
	public Claims getClaims(String token);

	/**
	 * Método que obtiene el username del token de un usuario autenticado.
	 * 
	 * @param token El token que contiene la información necesaria del usuario
	 *              autenticado
	 * @return El username contenido en el token.
	 */
	public String getUsername(String token);

	/**
	 * Método que obtiene los roles del token de un usuario autenticado.
	 * 
	 * @param token El token que contiene la información necesaria del usuario
	 *              autenticado
	 * @return Los roles contenidos en el token.
	 */
	public Collection<? extends GrantedAuthority> getRoles(String token) throws IOException;

	/**
	 * Método que resuelve el token con el fin de obtener el token sin el texto
	 * 'Bearer ' al inicio.
	 * 
	 * @param token El token que contiene la información necesaria del usuario
	 *              autenticado
	 * @return El token sin el texto 'Bearer ' al inicio.
	 */
	public String resolve(String token);
}
