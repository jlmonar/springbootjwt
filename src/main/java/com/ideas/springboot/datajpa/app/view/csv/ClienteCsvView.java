package com.ideas.springboot.datajpa.app.view.csv;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.AbstractView;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import com.ideas.springboot.datajpa.app.models.entity.Cliente;

/*
 * No es necesario colocar la extensión listar.csv, ya que tenemos una sola clase Component con el nombre
 * 'listar', si tuvieramos mas clases por ejm, listar.pdf, listar.json, listar.xml, entonces en esos casos
 * si debemos agregar la extensión.
 * Sin embargo, en este caso, como ejemplo agregamos la extensión csv que es la que estamos usando, pero 
 * para que funcione, debemos definir el media types en application.properties.
 */
@Component("listar.csv")
/*
 * No existe ninguna clase AbstractCsvView, por lo tanto debemos usar una clase
 * más especifica o abstracta, en este caso, AbstractView.
 */
public class ClienteCsvView extends AbstractView {

	public ClienteCsvView() {
		// Asignamos el mediaTypes. o mimeTypes
		setContentType("text/csv");
	}

	/*
	 * Configuramos este método ya que es un archivo que se descarga.
	 */
	@Override
	protected boolean generatesDownloadContent() {
		// ¿Genera un ocntenido que es descargable? SI
		return true;
	}

	/*
	 * Aqui envolvemos el contenido de nuestro archivo plano dentro de la respuesta.
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// Aquí cambio el nombre del archivo que se va a decargar.
		response.setHeader("Content Disposition", "attachment; filename=\"clientes.csv\"");
		// Aqui pasamos el content type a la respuesta.
		response.setContentType(getContentType());

		//Page<Cliente> clientes = (Page<Cliente>) model.get("clientes");
		List<Cliente> clientes = (List<Cliente>) model.get("clientes");

		/*
		 * Como primer parámetro se especifica el lugar donde queremos guardar el
		 * archivo plano, podemos crear un nuevo archivo con new
		 * FileWriter("target/writeWithCsvBeanWriter.csv") o bien podemos almacenar el
		 * archivo plano en la respuesta http, es decir, en el response. El segundo
		 * parámetro se refiere a las preferencias o configuraciones por defecto de
		 * nuestro archivo plano, esto es, como seran separadas las columnas, si por
		 * comma, tab, ';', etc, CsvPreference tiene varias opciones como
		 * STANDARD_PREFERENCE, EXCEL_PREFERENCE, EXCEL_NORTH_EUROPE_PREFERENCE se usa
		 * el que mas convenga según el caso.
		 */
		ICsvBeanWriter beanWriter = new CsvBeanWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

		/*
		 * En resumen, lo que beanwriter hace es tomar un bean, una clase entity por
		 * ejemplo con atributos y metodos getter y setter, como 'cliente' y eso lo
		 * convierte en una linea en el archivo plano. Perpo necesitamos que campos de
		 * esa clase entity vamos a incluir, entonces para ello definimos el header del
		 * archivo plano, el cual es un arreglo que contiene exactamente el nombre de
		 * los atributos de la clase entity que queremos incluir.
		 */
		String[] header = { "id", "nombre", "apellido", "email", "createAt" };
		beanWriter.writeHeader(header);

		for (Cliente cliente : clientes) {
			// Guardamos cada objeto cliente en el archivo plano.
			beanWriter.write(cliente, header);
		}

		// Cerramos el recurso.
		beanWriter.close();
	}

}
