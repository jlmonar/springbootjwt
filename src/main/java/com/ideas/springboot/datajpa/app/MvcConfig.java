package com.ideas.springboot.datajpa.app;

import java.nio.file.Paths;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/* Anotación que marca la clase como una clase de configuración. */
@Configuration
/*
 * WebMvcConfigurerAdapter es una clase que provee Springboot para poder
 * complementar y modificar o sobreescribir la configuración
 */
public class MvcConfig implements WebMvcConfigurer {

	private final Logger log = LoggerFactory.getLogger(getClass());

	/*
	 * Aqui estabamos agregando la carpeta 'uploads' como un recurso publico para
	 * poder accederlo en nuestras páginas, esta es uno forma de acceder al recurso
	 * 'uploads' para poder visualizarlo en la vista 'ver' del cliente
	 */
	/*
	 * @Override public void addResourceHandlers(ResourceHandlerRegistry registry) {
	 * super.addResourceHandlers(registry);
	 * 
	 * // toUri() lo que hace es tomar el path y le agrega el esquema file:/ String
	 * resourcePath = Paths.get("uploads").toAbsolutePath().toUri().toString();
	 * log.info("resourcePath: " + resourcePath);
	 * 
	 * //addResourceHandler registra el recurso 'uploads' al proyecto,
	 * addResourceLocations hace referencia //a la ruta donde se encuentra la
	 * carpeta 'uploads'. registry.addResourceHandler("/uploads/**")
	 * .addResourceLocations(resourcePath);
	 * //.addResourceLocations("file:/C:/Proyectos/Curso_spring_boot/uploads/"); }
	 */

	/*
	 * Hay 2 alternativas para manejar la vista de accesDenied, la primera es
	 * utilizar una Clase controller donde se implementa un metodo anotado con
	 * RequestMapping o GetMapping y le damos una url y una vista de error y
	 * personalizamos nustra página de error. Pero como es una página de error que
	 * no tiene ningún tipo de lógica, simplementa carga una vista entonces otra
	 * alternativa mas simple y fácil e simplementar un controlador parametizable,
	 * un viewController que simplemente cargue la vista, nada más.
	 */
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/error_403").setViewName("error_403");
	}

	/*
	 * Anotamos con @Bean para indicar que este objeto va a ser un componente )un
	 * Beans) de Spring y se tiene que registrar dentro del contenedor. Esta sería
	 * otra forma de resgistrar beans en el contenedor de Spring.
	 * 
	 * @Bean típicamente se usa para objetos instancias de otras clases propias del
	 * framework y @Component para nuestras propias clases.
	 */
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	/*
	 * Este primer Bean es el que resuelve el Locale, donde se va a almacenar el
	 * parametro de nuestro lenguaje y se va a guardar en la sesión. LocaleResolver
	 * se encarga del adaptador en el cual vamos a guardar nuestro locale, por
	 * ejemplo puede ser en la sesión http, en una cookie o en diferentes
	 * implementaciones
	 */
	@Bean
	public LocaleResolver localeResolver() {
		/*
		 * SessionLocaleResolver hace que se guarde el locale en la petición http cada
		 * vez que guardemos o modifiquemos un nuevo locale.
		 */
		SessionLocaleResolver localeResolver = new SessionLocaleResolver();
		localeResolver.setDefaultLocale(new Locale("es", "ES"));
		return localeResolver;
	}

	/*
	 * Para cambiar el locale cada vez que se envia el parametro del lenguaje, el
	 * nuevo idioma al que cambiarán los textos de nuestro sitio. Este segundo bean
	 * es el que se va a encargar de modificar o cambiar el lenguaje cada vez que
	 * pasemos el parámetro 'lang' por url. Un interceptor se ejecuta en cada
	 * petición o request y se encarga de realizar alguna tarea justo antes de
	 * invocar un método handler del controlador. En este caso justamente, si viene
	 * el parámetro del lenguaje, entonces va a tomar este nuevo valor y va a
	 * cambiar en la sesión el valor de nuestro locale para el idioma.
	 */
	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
		/*
		 * Cada vez que se pase por url a través del método get, por ejemplo el
		 * parametro lang con el valor es_ES se va a ajecutar este interceptor y va a
		 * realizar el cambio.
		 */
		localeChangeInterceptor.setParamName("lang");
		return localeChangeInterceptor;
	}

	/*
	 * Registramos nuestro LocaleChange en nuestra aplicación.
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(localeChangeInterceptor());
	}

	/*
	 * Bean que permite deserializar o convertir un objeto en XML, para ello
	 * configuramos el marshaling, para lo cual vamos a tener un método encargado de
	 * instanciar o crear este componente llamada Jaxb2Marshaller. Este es bean es
	 * importante ya que lo vamos a utilizar un nuestra vista xml para poder
	 * convertir la respuesta o contenido en un documento XML.
	 */
	@Bean
	public Jaxb2Marshaller jaxb2Marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		/*
		 * setClassesToBeBound son clases que vamos a convertir a XML, la clase root o
		 * la clase principal que va a tener otros atributos, dependencias, por lo tanto
		 * se va a transformar en un árbol de un documento XML según los campos o
		 * atributos que tengamos. Podemos enviar como parámetro un arreglo de nombres
		 * de clases que vamos a serializar o convertir a XML.
		 */
		marshaller.setClassesToBeBound(new Class[] {com.ideas.springboot.datajpa.app.view.xml.ClienteList.class});
		return marshaller;
	}

}
