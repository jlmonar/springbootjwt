package com.ideas.springboot.datajpa.app.models.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "facturas_items")
public class ItemFactura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private Integer cantidad;

	/*
	 * Aqui mapeamos muchos ItemsFactura a 1 Producto, el ItemFactura va a tener un
	 * solo Producto, la relación inversa NO la vamos a necesitar, ya que en un
	 * producto nunca vamos a querer listar los itemsFactura, simplemente mapeamos
	 * por el lado de ItemFactura y NO en Producto, por lo tanto, esta es una
	 * relación UNIDIRECCIONAL
	 */
	/*
	 * Por defecto, ya que estamos mapeando producto, se va a crear el atributo
	 * producto_id en la tabla facturas_items de forma automática, aunque podriamos
	 * especificar el nombre del campo de forma explicita usando la
	 * anotacion @JoinColumn, entonces de este modo, la llave foránea en la tabla
	 * facturas_items será el campo 'producto_id' que es para relacionar justamente
	 * la tabla ItemFactura con Producto, pero, de todas formas, podríamos omitir el
	 * uso de la anotación @JoinColumn ya que por defecto, al no indicar
	 * el @JoinColumn va a tomar el nombre del atributo 'Producto producto' y le va
	 * a concatenar '_id'.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "producto_id")
	/*
	 * Al querer mostrar las facturas en la lista de clientes en el formato json,
	 * hay un problema relacionado con la carga perezosa, ya que hay algunos
	 * atributos o propiedades que no se pueden serializar de forma correcta. La
	 * propiedad en cuestión es el atributo 'handler' en la clase Producto, sin
	 * embargo, podriamos pensar que Producto no tiene ningún atributo que se llama
	 * handler, sin embargo, si lo tiene, ya que no es el producto original sino es
	 * un proxy, ya que lo estamos cargando de forma perezosa a traves de LAZY, si
	 * bien, es un proxy que hereda de Producto y tiene algunos atributos extra como
	 * 'handler' e 'hibernateLazyInitializer'. Una posible solución para esto
	 * (solución poco práctica), la mas simple, es ir a la clase ItemFactura y
	 * modificar el fecthType LAZY por EAGER en el atributo Producto, este cambio lo
	 * que hará es traer los Productos inmediatamente y ya no en un proxy, sin
	 * embargo, como se decia, esta solución es poco práctica, ya que en muchas
	 * ocasiones necesitamos que sea carga peresoza(LAZY) porque no siempre vamos a
	 * utilizar el objeto de la relación, por lo cual es redundante y poco eficiente
	 * traer todas las consultas y todos los objetos de una sola vez cuando
	 * necesitamos en realidad mostrar solo algunos datos de una sola tabla y no de
	 * varias. Una solución mas eficiente para esto es dejar el fetchType como LAZY
	 * (tal como se lo tenía en un principio) y sobre el atributo 'producto' agregar
	 * la anotación JsonIgnoreProperties que lo que hará es ignorar algunos
	 * atributos o propiedades (los que se pasen como parámetro) del atributo
	 * producto en el JSON. Esta anotación bien puede ir sobre el atributo o sobre
	 * la clase Producto, en este caso lo dejamos sobre el atributo.
	 */
	@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
	private Producto producto;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Double calcularimporte() {
		return cantidad.doubleValue() * producto.getPrecio();
	}
}
