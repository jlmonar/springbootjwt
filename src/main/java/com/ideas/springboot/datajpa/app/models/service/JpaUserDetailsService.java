package com.ideas.springboot.datajpa.app.models.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.ideas.springboot.datajpa.app.models.dao.IUsuarioDAO;
import com.ideas.springboot.datajpa.app.models.entity.Role;
import com.ideas.springboot.datajpa.app.models.entity.Usuario;

/*
 * En esta clase Service no necesitamos implementar una interfaz propia, ya que la interfaz
 * la provee SpringSecurity, consiste en una interfaz propia de SpringSecurity para trabajar
 * con cualquier tipo de proveedor para implementar el proceso de autenticación ya sea 
 * a través de JPA, JDBC, etc.
 */
@Service("jpaUserDetailsService")
public class JpaUserDetailsService implements UserDetailsService {

	@Autowired
	private IUsuarioDAO usuarioDAO;

	private Logger logger = LoggerFactory.getLogger(JpaUserDetailsService.class);

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Usuario usuario = usuarioDAO.findByUsername(username);

		if (usuario == null) {
			logger.info("Error login: No existe usuario '" + username + "'");
			throw new UsernameNotFoundException("Username '" + username + "' no existe en el sistema.");
		} else {
			logger.info("USERNAME ES: '" + usuario.getUsername() + "'");
		}

		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

		for (Role role : usuario.getRoles()) {
			logger.info("Role: " + role.getAuthority());
			authorities.add(new SimpleGrantedAuthority(role.getAuthority()));
		}

		if (authorities.isEmpty()) {
			logger.error("Error login: usuario '" + username + "' no tiene roles asignados!");
			throw new UsernameNotFoundException("Error login: usuario '" + username + "' no tiene roles asignados!");
		}
		return new User(usuario.getUsername(), usuario.getPassword(), usuario.isEnabled(), true, true, true,
				authorities);
	}

}
