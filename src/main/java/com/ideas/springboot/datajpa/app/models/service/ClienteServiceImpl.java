package com.ideas.springboot.datajpa.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ideas.springboot.datajpa.app.models.dao.IClienteDAO;
import com.ideas.springboot.datajpa.app.models.dao.IFacturaDAO;
import com.ideas.springboot.datajpa.app.models.dao.IProductoDAO;
import com.ideas.springboot.datajpa.app.models.entity.Cliente;
import com.ideas.springboot.datajpa.app.models.entity.Factura;
import com.ideas.springboot.datajpa.app.models.entity.Producto;

/*
 * Una clase service está basada en el patrón de diseño FACADE, un único punto de acceso hacia distintos DAO o Repositorys.
 * Dentro de una clase Service podríamos tener como atributo y podriamos operar con diferentes clases DAO, asi evitamos tener que 
 * acceder de forma directa a las DAO dentro de los controladores.
 * La idea en la clase service es que por cada método en la clase DAO vamos a tener métodos en el ClienteService.
 * Tambien evitamos tener que usar las anotaciones Transactional en los DAO.
 * Incluso dentro de un método en la clase Service podemos interactuar con diferentes DAOs, y todos dentro de una
 * misma transacción, esa sería la idea.
 */
@Service
public class ClienteServiceImpl implements IClienteService {

	@Autowired
	private IClienteDAO clienteDAO;

	@Autowired
	private IProductoDAO productoDAO;

	@Autowired
	private IFacturaDAO facturaDAO;

	@Override
	@Transactional(readOnly = true)
	public List<Cliente> findAll() {
		return (List<Cliente>) clienteDAO.findAll();
	}

	@Override
	@Transactional
	public void save(Cliente cliente) {
		clienteDAO.save(cliente);
	}

	@Override
	@Transactional(readOnly = true)
	public Cliente findOne(Long id) {
		return clienteDAO.findById(id).orElse(null);
		// return clienteDAO.findById(id).orElse(null);//Para Spring Boot 2 (Spring 5)
	}

	@Override
	@Transactional
	public void delete(Long id) {
		clienteDAO.deleteById(id);
		// return clienteDAO.deleteById(id);//Para Spring Boot 2 (Spring 5)
	}

	@Override
	@Transactional(readOnly = true)
	public Page<Cliente> findAll(Pageable pageable) {
		return clienteDAO.findAll(pageable);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Producto> findByNombre(String term) {
		// return productoDAO.findByNombre(term);
		// return productoDAO.findByNombreLikeIgnoreCase("%" + term + "%");
		return productoDAO.findByNombreContainingIgnoreCase(term);
	}

	@Override
	@Transactional
	public void saveFactura(Factura factura) {
		facturaDAO.save(factura);
	}

	@Override
	@Transactional(readOnly = true)
	public Producto findProductoById(Long id) {
		return productoDAO.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public Factura findFacturaById(Long id) {
		return facturaDAO.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public void deleteFactura(Long id) {
		facturaDAO.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Factura fetchByIdWithClienteWithItemFacturaWithProducto(Long id) {
		return facturaDAO.fetchByIdWithClienteWithItemFacturaWithProducto(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Cliente fetchByIdWithFacturas(Long id) {
		return clienteDAO.fetchByIdWithFacturas(id);
	}

}
