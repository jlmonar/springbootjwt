package com.ideas.springboot.datajpa.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.ideas.springboot.datajpa.app.models.entity.Usuario;

public interface IUsuarioDAO extends CrudRepository<Usuario, Long> {

	public Usuario findByUsername(String username);
}
