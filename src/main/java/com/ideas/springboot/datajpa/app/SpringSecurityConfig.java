package com.ideas.springboot.datajpa.app;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.ideas.springboot.datajpa.app.auth.filter.JWTAuthenticationFilter;
import com.ideas.springboot.datajpa.app.auth.filter.JWTAuthorizationFilter;
import com.ideas.springboot.datajpa.app.auth.handler.LoginSuccessHandler;
import com.ideas.springboot.datajpa.app.auth.service.JWTService;
import com.ideas.springboot.datajpa.app.models.service.JpaUserDetailsService;

/*
 * Esta anotación es importante, ya que aqui habilitamos el uso de las anotaciones
 * @Secured y @PreAuthorize para dar seguridades.
 */
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private LoginSuccessHandler successHandler;

	@Autowired
	private DataSource dataSource;

	@Autowired
	private JpaUserDetailsService userDetailsService;

	@Autowired
	private BCryptPasswordEncoder passwordEncoder;

	/*
	 * Inyectamos este compoenente para luego poder ser utilizado en el contructor
	 * de los Filtros, se hace la inyección aqui y no directamente en los filtros ya
	 * que en los filtros no se pueden inyectar componentes.
	 */
	@Autowired
	private JWTService jwtService;

	/*
	 * Adaptamos la confIguración para trabajar con autenticación basada en Json Web
	 * Token, en el proyecto anterior spring-boot-data-jpa se estaba usando
	 * sesiones, la idea ahora es convertir esta configuración para que no se
	 * utilice sesiones y utilice la forma sin estado (STATELESS), para ello, lo
	 * primero que debemos hacer es deshabilitar la protección CSRF, deshabilitamos
	 * CSRF, por que vamos a usar JWT, por lo tanto, tenemos que asegurarnos también
	 * de que en las vistas no tengamos inputs csrf de forma explicita (en los
	 * formularios), si no arrojará errores. Luego que se deshabilita CSRF lo
	 * siguiente que hay que hacer es configurar SpringSecurity para habilitar la
	 * sesión en el SessionManagament como sin estado(STATELESS) por sobre el uso de
	 * sesiones (Estamos deshabilitando el uso de sesiones), por lo tanto, si
	 * intentamos iniciar sesión con el formulario de login, no va a funcionar, ya
	 * que no hay manejo de sesiones, es decir, nunca se va a realizar el login de
	 * forma persistente (con ESTADO o sesiones), por ejemplo, si hacemos sesión con
	 * el usuario admin, aparecerá el mensaje de login exitoso, pero no se mantiene
	 * persistente, no guarda los datos en la sesión, ni siquiera se mantienen los
	 * permisos ya que se mantiene deshabilitado SpringSecurity con sesiones en
	 * favor al uso de SpringSecurity en REST usando JWT.
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/", "/css/**", "/js/**", "/images/**", "/listar**", "/locale").permitAll()
				.anyRequest().authenticated()
				/*
				 * Deshabilitamos los formularios de login y logout, esto es porque trabajando
				 * sobre un proyecto API Rest, ya que al dejarlos habilitados, cada vez que se
				 * trate de acceder a un recurso no autorizado, o sin estar autenticado, nos
				 * redirigirá a la pantalla de login, para evitar esto, simplemente
				 * deshabilitamos formLogin, formLogout y también el accesDenied para el manejo
				 * de errores, esto lo hacemos ya que queremos que cada vez que se solicite un
				 * recurso no autorizado, nos arroje un error en tipo json, de manera que los
				 * errores se generen de forma automática con los códigos http, por ejemplo,
				 * 403, 401, 200, etc.
				 */
				/*
				 * .and() .formLogin() .successHandler(successHandler) .loginPage("/login")
				 * .permitAll() .and() .logout().permitAll() .and()
				 * .exceptionHandling().accessDeniedPage("/error_403")
				 */
				/*
				 * Para que el filtro funcione lo debemos registrar, y para ello enviamos como
				 * parametro una instancia de nuestro Filtro JWTAuthenticationFilter al cual
				 * debemos pasarle como parámetro el authenticationManager. Aqui no es necesario
				 * realizar una inyección de authenticationManager ya que la superclase
				 * abstracta WebSecurityConfigurerAdapter define un método que nos permite
				 * obtener el authenticationManager.
				 */
				.and().addFilter(new JWTAuthenticationFilter(authenticationManager(), jwtService))
				/*
				 * Registro el filtro que me permite verificar si un token enviado en una
				 * petición es valido o no.
				 */
				.addFilter(new JWTAuthorizationFilter(authenticationManager(), jwtService))
				/*
				 * Deshabilito la configuración CSRF
				 */
				.csrf().disable()
				/*
				 * Configuramos Spring Security habilitando la sesión en el SessionManagement
				 * como sin estado (STATELESS) por sobre el uso de sesiones
				 */
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

	}

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder build) throws Exception {
		/*
		 * Implementación de Spring Security con JPA.
		 */
		build.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);

		/*
		 * Implementación de Spring Security con JDBC Authentication. Aquí configuramos
		 * la consulta SQL para la autenticación, podemos customizar nustra consulta de
		 * inicio de sesión la cual seria una consulta sql nativa (nada que ver con
		 * JPA), usersByUsernameQuery usamos para obtener el usuario y
		 * authoritiesByUsernameQuery para obtener los roles.
		 */
		/*
		 * build.jdbcAuthentication() .dataSource(dataSource)
		 * .passwordEncoder(passwordEncoder)
		 * .usersByUsernameQuery("select username, password, enabled from users where username=?"
		 * )
		 * .authoritiesByUsernameQuery("select u.username, a.authority from authorities a inner join users u on (a.user_id = u.id) where u.username=?"
		 * );
		 */

		/*
		 * PasswordEncoder encoder =
		 * PasswordEncoderFactories.createDelegatingPasswordEncoder(); UserBuilder users
		 * = User.builder().passwordEncoder(encoder::encode);
		 */
		/*
		 * Vamos a atilizar un sistema de autentificación en memoria como un repositorio
		 * donde vamos a crear los usuarios con sus credenciales y roles.
		 */
		/*
		 * build.inMemoryAuthentication()
		 * .withUser(users.username("admin").password("12345").roles("ADMIN", "USER"))
		 * .withUser(users.username("jose").password("12345").roles("USER"));
		 */
	}
}
