package com.ideas.springboot.datajpa.app.auth.service;

import java.io.IOException;
import java.security.Key;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Logger;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ideas.springboot.datajpa.app.auth.SimpleGrantedAuthorityMixin;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

@Component
public class JWTServiceImpl implements JWTService {
	/*
	 * public: se puede acceder de forma pública. static: no hay necesidad de crear
	 * instancia para acceder a la constante, es un atributo de la clase, no del
	 * objeto. final: No se puede modificar.
	 */
	public static final Key SECRET_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS512);
	public static final long EXPIRATION_DATE = 3600000L * 4;
	public static final String TOKEN_PREFIX = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	private final static Logger LOGGER = Logger.getLogger(JWTService.class.getName());

	@Override
	public String create(Authentication auth) throws IOException {
		String username = auth.getName();
		// String username = ((User)authgetPrincipal()).getUsername();

		Collection<? extends GrantedAuthority> roles = auth.getAuthorities();

		/*
		 * No tenemos un setRoles o setAuthorities cuando queremos formar el
		 * JsonWebtoken, por lo tanto, como authorities es un dato extra que queremos
		 * incluir, decimos que son parte de los claims (o reclamaciones), entonces
		 * tenemos que obtener los Claims, y con el método put agregamos los nuevos
		 * atributos, 'authorities' en este caso. Pero hay que tener cuidado, ya que lo
		 * ideal es que se guarde como un texto o string, y en este caso 'roles' es un
		 * objeto, lo importante es que tenga un formato json, y para ello usamos el
		 * objeto ObjectMapper() para convertirlo a json.
		 */
		Claims claims = Jwts.claims();
		claims.put("authorities", new ObjectMapper().writeValueAsString(roles));

		/*
		 * Aquí se genera una llave secreta por nosotros, debemos especificar el tipo de
		 * algoritmo en el cual se va a encriptar nuestro SecretKey. Usamos HS512 ya que
		 * es el más utilizado, sin embargo, podemos utilizar cualquier algoritmo de
		 * encriptación. Pd: Dejamos el secretKey como constante de la clase.
		 */
		// Key secretKey = Keys.secretKeyFor(SignatureAlgorithm.HS512);
		LOGGER.info("SecretKey constant: " + SECRET_KEY);

		/*
		 * Aqui formamos el JsonWebToken a partir del username del usuario logueado y un
		 * secretKey, el cual es generado automaticamente por nosotros. Con el método
		 * signWith firmamos el token, para lo cual enviamos como parámetro el secretKey
		 * generado previamente.
		 */
		String token = Jwts.builder().setClaims(claims)// Agrego los roles del usuario autenticado en el token
				/*
				 * Agrego la fecha de creación del token.
				 */
				.setIssuedAt(new Date())
				/*
				 * Agrego fecha de expiración del token, el tiempo 3600000L corresponde a 1
				 * hora, multiplicado * 4 son 4 horas, ya que Date recibe como parametro un
				 * LONG, es de preferencia indicar que el 3600000 es un long con la L, asi nos
				 * aseguramos que estamos sumando dos LONG
				 */
				.setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_DATE)).setSubject(username)
				.signWith(SECRET_KEY).compact();
		return token;
	}

	@Override
	public boolean validate(String token) {

		try {
			/*
			 * Obtenemos los claims, ya que en este método se realiza la validación del
			 * token.
			 */
			getClaims(token);
			/*
			 * Si todo se lleva a cabo correctamente, entonces decimos que SI se trata de un
			 * TOKEN VALIDO.
			 */
			return true;
		} catch (JwtException | IllegalArgumentException e) {// Manejamos los distintos tipos de excepciones que se
																// pueden dar al momento de validar el token.
			/*
			 * Si la validación del token falla, ya sea por que es un token expirado, token
			 * malformado, token mal firmado, etc, entonces decimos que el token NO ES
			 * VALIDO.
			 */
			LOGGER.info("error" + e.getMessage());
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Claims getClaims(String token) {
		/*
		 * La variable 'Claims token' la utilizamos para guardar los datos del token
		 * VALIDO para posteriormente utilizarlos para la autenticación. Aqui
		 * implementamos la validación del token, para ello implementamos el método
		 * parser() para analizar el token a través de la misma clase que creó el token,
		 * la cual seria Jwts
		 */
		/*
		 * Asignamos la llave secreta con la cual generamos el token.
		 */
		Claims claims = Jwts.parser().setSigningKey((SECRET_KEY))
				/*
				 * Aqui lo que se hace es ANALIZAR el token, para lo cual tenemos que enviar
				 * como argumento el token, pero antes debemos resolver el token removiendo el
				 * texto q ue contiene al inicio "Bearer ".
				 */
				.parseClaimsJws(resolve(token))
				/*
				 * Aquí obtenemos los datos del token, es importante mencionar que getBody()
				 * solo sirve para obtener los datos del token, la validación en realidad se la
				 * hace con parser() y parserClaimsJws().
				 */
				.getBody();
		return claims;
	}

	@Override
	public String getUsername(String token) {
		return getClaims(token).getSubject();
	}

	@Override
	public Collection<? extends GrantedAuthority> getRoles(String token) throws IOException {
		Object roles = getClaims(token).get("authorities");

		/*
		 * Ya que estamos obteniendo los roles en formato json, debemos convertir estos
		 * al tipo Collection<? extends GrantedAuthority> ya que este es el tipo que
		 * acepta el constructor en UsernamePasswordAuthenticationToken, para ello,
		 * hacemos la conversión usando asList() y ObjectMapper(). El método readValue
		 * espera el contenido json de los roles como un String, sin embargo, se lo
		 * tiene como tipo Object , por lo cual también debemos hacer la conversión de
		 * los roles a String con toString() y getBytes()
		 */
		Collection<? extends GrantedAuthority> authorities = Arrays.asList(new ObjectMapper()
				/*
				 * Agregamos el mixIn, enviamos como primer argumento el nombre de la clase
				 * objetivo a la cual queremos convertir, y como segundo argumento enviamos la
				 * clase mixin
				 */
				.addMixIn(SimpleGrantedAuthority.class, SimpleGrantedAuthorityMixin.class)
				.readValue(roles.toString().getBytes(), SimpleGrantedAuthority[].class));
		return authorities;
	}

	@Override
	public String resolve(String token) {
		if (token != null && token.startsWith(TOKEN_PREFIX)) {
			/*
			 * Removemos el texto "Bearer " que se encuentra al inicio del token.
			 */
			return token.replace(TOKEN_PREFIX, "");
		}
		return null;
	}

}
