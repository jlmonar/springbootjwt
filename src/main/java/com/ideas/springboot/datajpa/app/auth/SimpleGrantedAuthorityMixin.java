package com.ideas.springboot.datajpa.app.auth;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/*
 * Esta clase MIXIN lo que hace es unificar, mezclar con SimpleGrantedAuthority para agregar
 * el constructor en la clase y se pueda crear este objeto, es decir, se pueda instanciar
 * de forma correcta.
 */
public abstract class SimpleGrantedAuthorityMixin {

	/*
	 * La anotación @JsonCreator es la marca para indicar a este constructor por
	 * defecto cuando se creen objetos authorities a partir de un JSON. Tenemos que
	 * pasar el String role al constructor, ya que así esta definido en el
	 * constructor de la clase SimpleGrantedAuthority, acá lo tenemos que dejar
	 * igual. Además debemos indicar que atributo del Json, que contiene el valor
	 * del role, va a inyectar en este constructor, en otras palabras, el atributo
	 * del json que va a corresponder al String role que vamos a inyectar en el
	 * constructor, es como un mapping, es decir, mapear el json con el atributo
	 * role de la clase SimpleGrantedAuthority, por defecto se llama 'authority' en
	 * el json, para indicar ese nombre del atributo en el json usamos la
	 * anotación @JsonProperty a un lado de String role y pasamos como argumento el
	 * nombre de la propiedad, 'authority' en este caso.
	 * 
	 */
	@JsonCreator
	public SimpleGrantedAuthorityMixin(@JsonProperty("authority") String role) {
	}

}
