package com.ideas.springboot.datajpa.app.controllers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.ideas.springboot.datajpa.app.models.entity.Cliente;
import com.ideas.springboot.datajpa.app.models.service.IClienteService;
import com.ideas.springboot.datajpa.app.models.service.IUploadFileService;
import com.ideas.springboot.datajpa.app.util.paginator.PageRender;
import com.ideas.springboot.datajpa.app.view.xml.ClienteList;

/* Anotación que marca la clase como un controlador. */
@Controller
/*
 * Se va a guardar en los atributos de la sesión el objeto cliente mapeado al
 * formulario cada que se invoca el crear o el editar con una petición GET, va a
 * obtener el objeto cliente, lo guarda en los atributos de la sesión y los pasa
 * a la vista, y la vista queda dentro de la sesión. Por lo tanto el id y todos
 * los datos del cliente quedan persistentes hasta que se envie al metodo
 * 'guardar', y en el método 'guardar' se debe eliminar eliminar la sesión con
 * SessionStatus
 * 
 */
@SessionAttributes("cliente")
public class ClienteController {
	protected final Log logger = LogFactory.getLog(this.getClass());

	/*
	 * Esta anotación va a buscar un componente registrado en el contenedor que
	 * implemente la interfaz IClienteService, lo encuentra y lo inyecta al
	 * atributo.
	 */
	@Autowired
	/*
	 * Esta anotación permite seleccionar que implementación de la interfaz en
	 * concreto vamos a inyectar
	 * 
	 * @Qualifier("ClienteDaoJPA")
	 */
	private IClienteService clienteService;

	@Autowired
	private IUploadFileService uploadFileService;

	/*
	 * A través de este objeto vamos a obtener el idioma.
	 */
	@Autowired
	private MessageSource messageSource;

	/*
	 * La expresión regular '.+' permiete que Spring no borre o no trunque la
	 * extensión del archivo, ya que cuando detecta que la url termina en un punto
	 * '.' y una extensión, la va a truncar cuando se envia un parámetro
	 */
	@Secured("ROLE_USER")
	@GetMapping(value = "/uploads/{filename:.+}")
	public ResponseEntity<Resource> verFoto(@PathVariable String filename) {
		Resource recurso = null;
		try {
			recurso = uploadFileService.load(filename);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + recurso.getFilename() + "\"")
				.body(recurso);
	}

	/*
	 * Con esta anotación estamos dando acceso al recurso a los usuarios con el rol
	 * de USER.
	 */
	@PreAuthorize("hasRole('ROLE_USER')")
	@GetMapping(value = "/ver/{id}")
	public String ver(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash,
			Locale locale) {
		Cliente cliente = clienteService.fetchByIdWithFacturas(id);// clienteService.findOne(id);

		if (cliente == null) {
			flash.addFlashAttribute("error", messageSource.getMessage("text.cliente.flash.db.error", null, locale));
			return "redirect:/listar";
		}

		model.put("cliente", cliente);
		model.put("titulo", messageSource.getMessage("text.cliente.detalle.titulo", null, locale).concat(": ")
				+ cliente.getNombre());

		return "ver";
	}

	/*
	 * Implemento un método handler del controloador que se encargue únicamente a
	 * renderizar una vista del tipo rest, ya sea json on xml.
	 */
	/*
	 * Lo importante en este método, es que la respuesta sea distinta, no del tipo
	 * HTML sino del tipo REST, y para que sea del tipo REST se debe anotar el
	 * método con la anotación @ResponseBody, la anotación puede ir arriba del
	 * método o luego de public. Lo que hace @ResponseBody es almacenar el listado
	 * de clientes en el cuerpo de la respuesta, y al guardarse de forma automática,
	 * SPRING va a deducir de que es un REST, por lo tanto puede ser un json o un
	 * xml. Cabe mencionar que la respuesta de este método no tendrá el atributo
	 * 'clientes' al inicio del json, de la forma en que tenía la respuesta del
	 * método 'listar' ya que ese correspondia al nombre que se le dió en la vista
	 * así model.addAttribute("clientes", clienteService.findAll());
	 */
	/*
	 * Al tratar de acceder a la ruta 'listar-rest' nos redirije a la página de
	 * login, es lógico porque estamos trabajando con SpringSecurity, y
	 * 'listar-rest' no está dentro del arreglo de los permisos en el método
	 * configure de la clase SpringSecurityConfig, para evitar este problema, lo
	 * agregamos.
	 */
	@GetMapping(value = "/listar-rest") // Cambio el nombre para evitar conflictos por rutas con el mismo nombre
	public @ResponseBody ClienteList listarRest() {
		/*
		 * Cambiamos el tipo de retorno a ClientList en caso de que queramos que el método retorne
		 * tanto 'xml' o 'json', pero si queremos solo formato 'json' podemos omitir la clasw wrapper ClientList
		 * y retornar simplemente un List<Cliente>
		 */
		return new ClienteList(clienteService.findAll());
	}

	/*
	 * Usando '{"/listar", "/"}' estamos diciendo que 'listar' va a ser la página de
	 * inicio.
	 */
	@RequestMapping(value = { "/listar", "/" }, method = RequestMethod.GET)
	public String listar(@RequestParam(name = "page", defaultValue = "0") int page, Model model,
			Authentication authentication, HttpServletRequest request, Locale locale,
			@RequestParam(name = "format", defaultValue = "html") String format) {

		if (authentication != null) {
			logger.info("Hola usuario autenticado, tu username es: ".concat(authentication.getName()));
		}

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			logger.info(
					"Utilizando forma estática SecurityContextHolder.getContext().getAuthentication(): Hola usuario autenticado, tu username es: "
							.concat(auth.getName()));
		}

		if (hasRole("ROLE_ADMIN")) {
			logger.info("Hola " + auth.getName() + " tienes acceso!");
		} else {
			logger.info("Hola " + auth.getName() + " NO tienes acceso!");
		}

		SecurityContextHolderAwareRequestWrapper securityContext = new SecurityContextHolderAwareRequestWrapper(request,
				"ROLE_");
		if (securityContext.isUserInRole("ADMIN")) {
			logger.info("Usando forma SecurityContextHolderAwareRequestWrapper: Hola " + auth.getName()
					+ " tienes acceso!");
		} else {
			logger.info("Usando forma SecurityContextHolderAwareRequestWrapper: Hola " + auth.getName()
					+ " NO tienes acceso!");
		}

		if (request.isUserInRole("ADMIN")) {
			logger.info("Usando forma HttpServletRequest: Hola " + auth.getName() + " tienes acceso!");
		} else {
			logger.info("Usando forma HttpServletRequest: Hola " + auth.getName() + " NO tienes acceso!");
		}

		/*
		 * Según el formato, decidimos si paginamos u obtenemos todos los clientes para
		 * mostrarlos en el archivo plano csv.
		 */
		if (format.equals("html")) {
			Pageable pageRequest = PageRequest.of(page, 4);
			// Pageable pageRequest = PageRequest.off(page, size); // Para Spring Boot 2
			// (Spring 5)
			Page<Cliente> clientes = clienteService.findAll(pageRequest);

			PageRender<Cliente> pageRender = new PageRender<>("/listar", clientes);
			model.addAttribute("titulo", messageSource.getMessage("text.cliente.listar.titulo", null, locale));
			model.addAttribute("clientes", clientes);
			model.addAttribute("page", pageRender);
		} else if (format.equals("json")) {
			Pageable pageRequest = PageRequest.of(page, 4);
			Page<Cliente> clientes = clienteService.findAll(pageRequest);

			/*
			 * Se usa el 'clientes' paginable si queremos mostrar en el json los clientes
			 * egún la á pagina en la que estamos, essto agregara a la estructura json
			 * atributos como pageable y sort
			 */
			// model.addAttribute("clientes", clientes);
			model.addAttribute("clientes", clienteService.findAll());
		} else {
			model.addAttribute("clientes", clienteService.findAll());
		}

		return "listar";
	}

	/* Es lo mismo utilizar Map<String, Object> que Model */
	/*
	 * Con esta anotación estamos dando acceso al recurso solo a los usuarios con el
	 * rol de ADMIN.
	 */
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/form")
	// public String crear(Map<String, Object> model) {
	public String crear(Model model, Locale locale) {
		Cliente cliente = new Cliente();
		/*
		 * model.put("titulo", "Formulario de cliente"); model.put("cliente", cliente);
		 */
		model.addAttribute("titulo", messageSource.getMessage("text.cliente.form.titulo", null, locale));
		model.addAttribute("cliente", cliente);
		return "form";
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "/form/{id}")
	/* La anotación PathVariable inyecta el valor del parámetro de la ruta */
	public String editar(@PathVariable(value = "id") Long id, Map<String, Object> model, RedirectAttributes flash,
			Locale locale) {
		Cliente cliente = null;
		if (id > 0) {
			cliente = clienteService.findOne(id);

			if (cliente == null) {
				flash.addFlashAttribute("error", messageSource.getMessage("text.cliente.flash.db.error", null, locale));
				return "redirect:/listar";
			}
		} else {
			flash.addFlashAttribute("error", messageSource.getMessage("text.cliente.flash.id.error", null, locale));
			return "redirect:/listar";
		}
		model.put("cliente", cliente);
		model.put("titulo", messageSource.getMessage("text.cliente.form.titulo.editar", null, locale));
		return "form";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/form", method = RequestMethod.POST)
	public String guardar(@Valid Cliente cliente, BindingResult result, Model model,
			@RequestParam("file") MultipartFile foto, RedirectAttributes flash, SessionStatus status, Locale locale) {
		model.addAttribute("titulo", messageSource.getMessage("text.cliente.form.titulo", null, locale));
		/* Validamos si el objeto cliente que llegó del formulario está con errores. */
		if (result.hasErrors()) {
			return "form";
		}

		if (!foto.isEmpty()) {
			/*
			 * Path directorioRecursos = Paths.get("src//main//resources//static/uploads");
			 * String rootPath = directorioRecursos.toFile().getAbsolutePath();
			 */
			/*
			 * String rootPath = "C://Temp//uploads";
			 */

			/*
			 * Eliminamos foto solo si se trata de edición de un cliente y este ya tenía una
			 * imagen previa.
			 */
			if (cliente.getId() != null && cliente.getId() > 0 && cliente.getFoto() != null
					&& cliente.getFoto().length() > 0) {
				uploadFileService.delete(cliente.getFoto());
			}

			String uniqueFilename = null;
			try {
				uniqueFilename = uploadFileService.copy(foto);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			flash.addFlashAttribute("info",
					messageSource.getMessage("text.cliente.flash.foto.subir.success", null, locale) + uniqueFilename
							+ "'");
			cliente.setFoto(uniqueFilename);
		}

		String mensajeFlash = (cliente.getId() != null)
				? messageSource.getMessage("text.cliente.flash.editar.success", null, locale)
				: messageSource.getMessage("text.cliente.flash.crear.success", null, locale);

		clienteService.save(cliente);
		status.setComplete();// Luego de guardar, setComplete va a aliminar el objeto cliente de la sesión.
		flash.addFlashAttribute("success", mensajeFlash);
		return "redirect:listar";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/delete/{id}")
	public String eliminar(@PathVariable(value = "id") Long id, RedirectAttributes flash, Locale locale) {
		if (id > 0) {
			Cliente cliente = clienteService.findOne(id);
			clienteService.delete(id);
			flash.addFlashAttribute("success",
					messageSource.getMessage("text.cliente.flash.eliminar.success", null, locale));

			if (uploadFileService.delete(cliente.getFoto())) {
				String mensajeFotoEliminar = String.format(
						messageSource.getMessage("text.cliente.flash.foto.eliminar.success", null, locale),
						cliente.getFoto());
				flash.addFlashAttribute("info", mensajeFotoEliminar);
			}
		}
		return "redirect:/listar";
	}

	private boolean hasRole(String role) {
		SecurityContext context = SecurityContextHolder.getContext();

		if (context == null) {
			return false;
		}

		Authentication auth = context.getAuthentication();

		if (auth == null) {
			return false;
		}

		/*
		 * <? extends GrantedAuthority> Es una colección de cualquier tipo de objeto que
		 * implemente o herede de GrantedAuthority
		 */
		Collection<? extends GrantedAuthority> authorities = auth.getAuthorities();

		/*
		 * Otra forma de validar los roles. El método contains(GrantedAuthority) retorna
		 * un booleano si contiene o no el elemento en la colección
		 */
		return authorities.contains(new SimpleGrantedAuthority(role));
		/*
		 * for (GrantedAuthority authority : authorities) { if
		 * (role.equals(authority.getAuthority())) { logger.info("Hola usuario " +
		 * auth.getName() + ", tu rol es: " + authority.getAuthority()); return true; }
		 * } return false;
		 */
	}
}
