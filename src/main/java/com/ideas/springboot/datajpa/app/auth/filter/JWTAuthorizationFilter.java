package com.ideas.springboot.datajpa.app.auth.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import com.ideas.springboot.datajpa.app.auth.service.JWTService;
import com.ideas.springboot.datajpa.app.auth.service.JWTServiceImpl;

/*
 * Este filtro lo que hace es tomar el token que viene en el header de las peticiones, lo analice y verifique
 * si es valido. Este filtro, a diferencia del otro (JWTAuthenticationFilter), se ejecuta en cada request, 
 * siempre y cuando enviamos un Bearer en el header, por lo tanto este filtro debe validar que dentro del
 * header venga el Authorization y el Bearer, si viene, entonces se tomará el token y se lo analizará para
 * verificar si es valido y puede acceder al recurso que está solicitando en la petición, de lo contrario, si 
 * en el header no viene el Authorization ni el Bearer, entonces simplemente se salta este filtro.
 * Este filtro hereda de la superclase BasicAuthenticationFilter que a su vez hereda de la clase OncePerRequestFilter,
 * es decir, se va a ejecutar en cada request.
 */
public class JWTAuthorizationFilter extends BasicAuthenticationFilter {
	private JWTService jwtService;

	public JWTAuthorizationFilter(AuthenticationManager authenticationManager, JWTService jwtService) {
		super(authenticationManager);
		this.jwtService = jwtService;
	}

	/*
	 * Sobreesribimos el método doFilterInternal ya que aquí es donde vamos a
	 * validar si el token enviado por el usuario en el header de la petición es
	 * valido o no.
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String header = request.getHeader(JWTServiceImpl.HEADER_STRING);

		/*
		 * Si el header(Authorization) es nulo o si NO inicia con "Bearer ", entonces
		 * automáticamente continuamos con la ejecución de los demás filtros y el
		 * request, y por supuesto nos salimos del filtro actual, no continuamos con la
		 * ejecución.
		 */
		if (!requiresAuthentication(header)) {
			chain.doFilter(request, response);
			return;
		}

		/*
		 * Siempre, cada vez que queremos acceder a un recurso, debemos autenticarnos
		 * con el token que fue generado cuando se hizo el login. Aqui solamente lo
		 * definimos como nulo.
		 */
		UsernamePasswordAuthenticationToken authentication = null;

		/*
		 * Manejamos la autenticación cuando el token es válido. En este caso usamos el
		 * método validate de jwtService para determinar si es un token valido o no.
		 */
		if (jwtService.validate(header)) {
			logger.info("El token es valido");
			/*
			 * Aqui realizamos el inicio de sesión, enviamos como parametro el username, las
			 * credenciales en nulo, y finalmente los roles o authorities.
			 */
			authentication = new UsernamePasswordAuthenticationToken(jwtService.getUsername(header), null,
					jwtService.getRoles(header));
		}

		/*
		 * SecurityContext se encarga de manejar el contexto de seguridad, lo que se
		 * hace acá es asignar el objeto authentication dentro del contexto, esto
		 * autentica al usuario dentro del request o petición, en este caso no estamos
		 * usando sesiones, por lo tanto queda autenticado dentro de la solicitud.
		 */
		SecurityContextHolder.getContext().setAuthentication(authentication);

		/*
		 * Continuamos con la cadena de ejecución del request, tanto para los otros
		 * filtros, para los servlets y controladores de Spring.
		 */
		chain.doFilter(request, response);
	}

	protected boolean requiresAuthentication(String header) {
		if (header == null || !header.startsWith(JWTServiceImpl.TOKEN_PREFIX)) {
			return false;
		}
		return true;
	}
}
