package com.ideas.springboot.datajpa.app.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ideas.springboot.datajpa.app.models.entity.Cliente;

/*
 * CrudRepository se puede reutilizar con diferentes clases entity.
 * CrudRepositiry es una interfaz propia de SpringData que implementa diferentes métodos
 * save, findOne, find All, etc. Basicamente, automatiza bastante la implementación del DAO
 * ya que solamente necesitamos implementar la interfaz que hereda de CrudRepository.
 * 
 * También podríamos extender de JPA, que además del CRUD también implementa otras cosas, como
 * por ejemplo, para paginar resultados, hay varias características anexas.
 */
public interface IClienteDAO extends JpaRepository<Cliente, Long> {
	/*
	 * public List<Cliente> findAll(); public void save(Cliente cliente); public
	 * Cliente findOne(Long id); public void delete(Long id);
	 */

	/*
	 * select
	 * 
	 * cliente0_.id as id1_0_0_, facturas1_.id as id1_1_1_, cliente0_.apellido as
	 * apellido2_0_0_, cliente0_.create_at as create_a3_0_0_, cliente0_.email as
	 * email4_0_0_, cliente0_.foto as foto5_0_0_, cliente0_.nombre as nombre6_0_0_,
	 * facturas1_.cliente_id as cliente_5_1_1_, facturas1_.create_at as
	 * create_a2_1_1_, facturas1_.descripcion as descripc3_1_1_,
	 * facturas1_.observacion as observac4_1_1_, facturas1_.cliente_id as
	 * cliente_5_1_0__, facturas1_.id as id1_1_0__
	 * 
	 * from clientes cliente0_ left outer join facturas facturas1_ on
	 * cliente0_.id=facturas1_.cliente_id
	 * 
	 * where cliente0_.id=?
	 * 
	 * Si usamos solo join fetch, entonces estamos haciendo un inner join, por lo
	 * tanto, va a traer los datos del cliente siempre y cuando exista la relación
	 * en la otra tabla, es decir, que exista el registro en la tabla factura, por
	 * ende, si usamos el query solo como 'join fetch' no va a arrojar resultados
	 * del cliente si en la ralacion no tiene ninguna factura, y ese no es el
	 * resultado que queremos, ya que queremos que a se muestren los datos del
	 * cliente sin importar si tiene o no facturas. Para resolver ese problema
	 * usamos 'left join fetch' que trae los datos del cliente sin importar si tiene
	 * o no facturas.
	 */
	@Query("select c from Cliente c left join fetch c.facturas where c.id = ?1")
	public Cliente fetchByIdWithFacturas(Long id);
}
