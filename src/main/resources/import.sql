/* Creamos algunos clientes. */
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (1, 'José', 'Monar', 'jlmonar@mail.com', '2018-09-25', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (2, 'Andrés', 'Perex', 'aguzman@mail.com', '2018-09-28', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (3, 'user', 'Guzman', 'aguzman@mail.com', '2018-09-28', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (4, 'ariel', 'Guzman', 'aguzman@mail.com', '2018-09-28', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (5, 'julio', 'edd', 'aguzman@mail.com', '2018-09-28', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (6, 'Juan', 'sdsf', 'aguzman@mail.com', '2018-09-28', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (7, 'Andrés', 'Guzman', 'aguzman@mail.com', '2018-09-28', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (8, 'carlos', 'yyyuhh', 'aguzman@mail.com', '2018-09-28', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (9, 'Andrés', 'iiiiii', 'aguzman@mail.com', '2018-09-28', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (10, 'ANa', 'Guzman', 'aguzman@mail.com', '2018-09-28', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (11, 'Andrés', 'ppop', 'aguzman@mail.com', '2018-09-28', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (12, 'Andrés', 'dvvdfv', 'aguzman@mail.com', '2018-09-28', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (13, 'pedro', 'Guzman', 'aguzman@mail.com', '2018-09-28', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (14, 'Andrés', 'guueueu', 'aguzman@mail.com', '2018-09-28', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (15, 'juan', 'Guzman', 'aguzman@mail.com', '2018-09-28', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (16, 'Andrés', 'Guzman', 'aguzman@mail.com', '2018-09-28', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (17, 'carl', 'Monar', 'jlmonar@mail.com', '2018-09-25', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (18, 'qqqqqsqsqs', 'Monar', 'jlmonar@mail.com', '2018-09-25', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (19, 'and', 'andrade', 'jlmonar@mail.com', '2018-09-25', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (20, 'José', 'Monar', 'jlmonar@mail.com', '2018-09-25', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (21, 'jaime', 'Monar', 'jlmonar@mail.com', '2018-09-25', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (22, 'jhon', 'Monar', 'jlmonar@mail.com', '2018-09-25', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (23, 'Jousertestsé', 'Monar', 'jlmonar@mail.com', '2018-09-25', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (24, 'test', 'Monar', 'jlmonar@mail.com', '2018-09-25', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (25, 'José', 'Monar', 'jlmonar@mail.com', '2018-09-25', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (26, 'uuuuuy', 'Monar', 'jlmonar@mail.com', '2018-09-25', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (27, 'dfddf', 'Monar', 'jlmonar@mail.com', '2018-09-25', '');
INSERT INTO clientes (id,nombre,apellido,email,create_at, foto) VALUES (28, 'cristhian', 'Monar', 'jlmonar@mail.com', '2018-09-25', '');

/* Creamos algunos productos. */
INSERT INTO productos (nombre, precio, create_at) VALUES ('Panasonic pantalla LCD', 599.99, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES ('Cámara Sony', 899.99, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES ('iPod Apple', 400, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES ('Sony Notebook', 350, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES ('Mica celular', 5, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES ('Bicicleta ARO 26', 250, NOW());
INSERT INTO productos (nombre, precio, create_at) VALUES ('Grabadora de voz', 300, NOW());

/* Creamos algunas facturas. */
INSERT INTO facturas(descripcion, observacion, cliente_id, create_at) VALUES ('Factura equipos de oficina.', null, 1, NOW());
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES (1, 1, 1);
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES (2, 1, 4);
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES (1, 1, 5);
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES (1, 1, 7);

INSERT INTO facturas(descripcion, observacion, cliente_id, create_at) VALUES ('Factura bicicleta', 'Nota importante!', 1, NOW());
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES (3, 2, 6);

/* Creamos algunos usuarios con sus roles. */
INSERT INTO users (username, password, enabled) VALUES('jose', '$2a$10$O9wxmH/AeyZZzIS09Wp8YOEMvFnbRVJ8B4dmAMVSGloR62lj.yqXG', 1);
INSERT INTO users (username, password, enabled) VALUES('admin', '$2a$10$DOMDxjYyfZ/e7RcBfUpzqeaCs8pLgcizuiQWXPkU35nOhZlFcE9MS', 1);

INSERT INTO authorities (user_id, authority) VALUES(1, 'ROLE_USER');
INSERT INTO authorities (user_id, authority) VALUES(2, 'ROLE_USER');
INSERT INTO authorities (user_id, authority) VALUES(2, 'ROLE_ADMIN');
